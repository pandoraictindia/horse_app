<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Emailer</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no"> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: calibri; margin: 0 auto;border:1px solid #ccc; padding:20px;
border-radius: 5px">
	<tr>
		<td colspan="2">Hello <b><?php echo $detail['username']; ?>,</b></td>
	</tr>

	<tr><td colspan="2"><h2>Welcome to MYHest</h2></td></tr>

	<tr>
		<td colspan="2">
			<p style="font-weight:500">You recently requested to reset your password for your MyHest account.</p>
		</td>
	</tr>

	<tr><td colspan="2"><p style="font-weight:500">Click the below button to reset it :</p></td></tr>

	<tr><td colspan="2"><a style="color:red;" href="<?php echo $detail['reset_pass_url']; ?>">Reset Your Password</a></td></tr>

	<tr><td colspan="2"><p style="font-weight:500">If you do not request a password reset. please ignore this email or contact support if any query.</p></td></tr>

	<tr><td colspan="2" style="font-weight: 600;padding-top:30px">Thanks & Regards,</td></tr>

	<tr>
		<td colspan="2">
			MyHest Team<br>
			<!--<img src="<?php echo $detail['logo_url']; ?>"  width="120" border="0" alt="" style="display: block;">-->
		</td>
	</tr>
</table>
</body>
</html>

