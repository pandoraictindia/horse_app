<?php
//require(APPPATH.'models/Go_model.php');
class Login_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function checkUserLogin($data, &$errormessage)
	{
		$result = array();
		$where_condition = array('email'=>$data['email'],'password'=>$data['password'],'active'=>'1','type'=>'1');
		$this->db->select('id,first_name,last_name,email,subscribe_status,phone_no');
		$this->db->from('user');
		$this->db->where($where_condition);
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Wrong email or password.";
		}
		return $result;
	}

	public function addUserSession($data, &$errormessage)
	{
		$result = 0;
		//check exists
		$exists = $this->db->get_where('user_session',array('user_id' =>$data['user_id']))->row_array();
		if(empty($exists))
		{
			$result = $this->db->insert('user_session',array('user_id' =>$data['user_id'],'authcode'=>$data['authcode']));
		}
		else
		{
			$this->db->where('user_id',$data['user_id']);
			$result = $this->db->update('user_session',array('authcode'=>$data['authcode']));
		}
		//return result
		if($result == 0)
		{
			$errormessage = "Something Wrong. Please try again.";
		}
		return $result;
	}

	public function checkEmail($email, &$errormessage)
	{
		$result = array();
		$this->db->select('id,first_name,last_name,email');
		$this->db->from('user');
		$this->db->where('email',$email);
		$result = $this->db->get()->row_array();
		
		if(empty($result))
		{
			$errormessage = "Email not exists.";
		}

		return $result;
	}

	public function addForgetPassToken($email,$token, &$errormessage)
	{
		$result = 0;
		$this->db->where('email',$email);
		$result = $this->db->update('user',array('forget_pass_token' => $token));
		if($result == 0)
		{
			$errormessage = "Something went wrong.Try again.";
		}
		return $result;
	}

	public function resetPassword($data, &$errormessage)
	{
		$result = 0;
		$user = $this->db->get_where('user',array('forget_pass_token'=>$data['token']))->row_array();
		if(!empty($user))
		{
			$this->db->where('id',$user['id']);
			$result = $this->db->update('user',array('password' => md5($data['newpass'])));
			if($result == 0)
			{
				$errormessage = "Something went wrong.Try again.";
			}
		}		
		else
		{
			$errormessage = "Invalid url";
		}

		return $result;
	}

	public function changePassword($data, &$errormessage)
	{
		$result = 0;
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('id',$data['user_id']);
		$this->db->where('password',md5($data['oldpass']));
		$this->db->where('active','1');
		$exists = $this->db->get()->row_array();
		if(empty($exists))
		{
			$errormessage = "Old password not match.";
		}
		else
		{
			$this->db->where('id',$data['user_id']);
			$result = $this->db->update('user',array('password' => md5($data['newpass'])));
			if($result !== 1)
			{
				$errormessage = "Password not changed. Please try again.";
			}
		}

		return $result;
	}

	public function getHorsephotos($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('id as photo_id,photo,caption');
		$this->db->from('horse_photos');
		$this->db->where('horse_id',$horse_id);
		$this->db->where('active','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Photos not available";
		}
		return $result;
	}
	
	public function getMyHorse($user_id, &$errormessage)
	{
		$result = array();
		$query = $this->db->query("SELECT -1 as share_flag,NULL as shared_id, `id`, `horse_name`, `birthdate`, `remark`, `profile_photo` FROM `horses` WHERE `active` = '1' AND `user_id` = '$user_id' UNION (SELECT a.share_flag,a.id as shared_id,b.id, b.horse_name, b.birthdate,b.birthdate,b.profile_photo FROM shared_horse a INNER JOIN horses b ON b.id = a.horse_id WHERE a.active = '1'AND a.user_id = '$user_id') ORDER BY id DESC");
		$result = $query->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getHorseAppoinments($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number');
		$this->db->from('horse_appoinements a');
		$this->db->join('horses b', 'a.horse_id = b.id');
		$this->db->join('contacts c', 'a.contact_name = c.id','left');
		$this->db->where('a.active','1');
		$this->db->where('a.horse_id',(Int)$horse_id);	
		// $this->db->order_by('a.date','DESC');
		// $this->db->order_by('a.time','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getFilterHorseAppoinments($data, &$errormessage)
	{
		$horse_id = $data['horse_id'];
		$result = array();
		// today
		if((Int)$data['filter_type'] == 1)
		{
			$today = $data['today'];
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.horse_id = '$horse_id' AND a.date = '$today' ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}

		// week
		if((Int)$data['filter_type'] == 2)
		{
			$from_date = date('Y-m-d',strtotime($data['start_date']));
			$to_date = date('Y-m-d',strtotime($data['end_date']));
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.horse_id = '$horse_id' AND (`date` BETWEEN '$from_date' AND '$to_date') ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}

		// month year
		if((Int)$data['filter_type'] == 3)
		{
			$month = $data['month'];
			$year = $data['year'];
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.horse_id = '$horse_id' AND month(date)='$month' AND year(date)='$year' ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}

		// custom
		if((Int)$data['filter_type'] == 4)
		{
			$from_date = date('Y-m-d',strtotime($data['start_date']));
			$to_date = date('Y-m-d',strtotime($data['end_date']));
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.horse_id = '$horse_id' AND (`date` BETWEEN '$from_date' AND '$to_date') ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}
		
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getAllHorseAppoinments($user_id, &$errormessage)
	{
		$result = array();

		$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number, -1 as share_flag FROM horse_appoinements a INNER JOIN horses b ON a.horse_id = b.id LEFT JOIN contacts c ON a.contact_name = c.id WHERE a.active = '1' AND a.user_id = '$user_id'
				UNION
				select a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number,sh.share_flag
				from horse_appoinements a 
				inner join shared_horse sh on a.horse_id = sh.horse_id and sh.active ='1'
				INNER JOIN horses b ON a.horse_id = b.id
				LEFT JOIN contacts c ON a.contact_name = c.id
				where sh.user_id = '$user_id' AND a.active = '1'
				");
		$result = $query->result_array();

		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getAllFilterHorseAppoinments($data, &$errormessage)
	{
		$user_id = $data['user_id'];
		$result = array();
		// today
		if((Int)$data['filter_type'] == 1)
		{
			$today = $data['today'];
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.user_id = '$user_id' AND a.date = '$today' ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}

		// week
		if((Int)$data['filter_type'] == 2)
		{
			$from_date = date('Y-m-d',strtotime($data['start_date']));
			$to_date = date('Y-m-d',strtotime($data['end_date']));
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.user_id = '$user_id' AND (`date` BETWEEN '$from_date' AND '$to_date') ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}

		// month year
		if((Int)$data['filter_type'] == 3)
		{
			$month = $data['month'];
			$year = $data['year'];
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.user_id = '$user_id' AND month(date)='$month' AND year(date)='$year' ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}

		// custom
		if((Int)$data['filter_type'] == 4)
		{
			$from_date = date('Y-m-d',strtotime($data['start_date']));
			$to_date = date('Y-m-d',strtotime($data['end_date']));
			$query = $this->db->query("SELECT a.*,a.contact_name as contact_id,b.horse_name,b.profile_photo,c.contact_name,c.contact_number FROM horse_appoinements a INNER JOIN horses b ON b.id = a.horse_id LEFT JOIN contacts c ON c.id = a.contact_name WHERE a.active = '1' AND a.user_id = '$user_id' AND (`date` BETWEEN '$from_date' AND '$to_date') ORDER BY a.date DESC,a.time DESC");
			$result = $query->result_array();
		}
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getappoinmentphotos($appoinemnt_id,&$errormessage)
	{
		$result = array();
		$this->db->select('id as photo_id,photo');
		$this->db->from('appoinments_photos');
		$this->db->where('appoinment_id',$appoinemnt_id);
		$this->db->where('active','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Photos not available";
		}
		return $result;
	}

	public function getOtherDetail($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('id,live_no,microchip_no');
		$this->db->from('horses');
		$this->db->where('id',$horse_id);
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}

	public function getParentDetail($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('id,mother_name,father_name');
		$this->db->from('horses');
		$this->db->where('id',$horse_id);
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}

	public function getFoodDetail($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('food_detail');
		$this->db->where('horse_id',$horse_id);
		$this->db->where('active','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}

	public function getContacts($horse_id,&$errormessage)
	{
		$result = array();

		$this->db->select('b.*');
		$this->db->from('horse_contact a');
		$this->db->join('contacts b','a.contact_id = b.id');
		$this->db->where('a.horse_id',$horse_id);
		$this->db->where('b.active','1');
		$this->db->order_by('b.id','DESC');
		$result = $this->db->get()->result_array();

		// $this->db->select('*');
		// $this->db->from('contacts');
		// $this->db->where('horse_id',$horse_id);
		// $this->db->where('active','1');
		// $result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}
	
	public function getContactsByUser($user_id,&$errormessage)
	{
		$result = array();

		// $query = $this->db->query("SELECT * FROM contacts WHERE active = '1' AND user_id = '$user_id' ORDER BY id DESC");
		$query = $this->db->query("SELECT c.*, -1 as share_flag from horse_contact hc 
									inner join horses h on hc.horse_id = h.id and h.active ='1'
									 inner join contacts c on hc.contact_id= c.id
									where h.user_id ='$user_id' and c.active = '1'
									union
									SELECT c.*, sh.share_flag from horse_contact hc 
									inner join shared_horse sh on hc.horse_id = sh.horse_id and sh.active ='1'
									inner join contacts c on hc.contact_id= c.id
									where sh.user_id = '$user_id' and c.active = '1' ORDER BY id DESC ");
		$result = $query->result_array();
		
		if(!empty($result))
		{
			for($i=0;$i<count($result);$i++) {
				$result[$i]['horse_ids'] = [];
				$result[$i]['horse_names'] = '';
				$horse_name = [];
				//get horses
				$this->db->select('a.horse_id,b.horse_name');
				$this->db->from('horse_contact a');
				$this->db->join('horses b', 'a.horse_id = b.id');
				$this->db->where('a.contact_id',$result[$i]['id']);
				$this->db->where('b.active','1');
				$horse_ids = $this->db->get()->result_array();
				if(!empty($horse_ids))
				{
					for($j=0;$j<count($horse_ids);$j++) {
						array_push($result[$i]['horse_ids'], $horse_ids[$j]['horse_id']);
						array_push($horse_name, $horse_ids[$j]['horse_name']);
						$result[$i]['horse_names'] = implode(',', $horse_name);
					}
				}				
			}
		}
		else
		{
			$errormessage = "Record not available";
		}
		return $result;
	}

	public function getUserDetail($user_id,&$errormessage)
	{
		$result = array();
		$this->db->select('id,first_name,last_name,email,subscribe_status,phone_no');
		$this->db->from('user');
		$this->db->where('id',$user_id);
		$this->db->where('active','1');
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}
	
	public function getHorseDetail($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('horses');
		$this->db->where('id',$horse_id);
		$this->db->where('active','1');
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}
	
	public function getHorseLocation($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('locations');
		$this->db->where('horse_id',$horse_id);
		$this->db->where('active','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available";
		}
		return $result;
	}
	
}