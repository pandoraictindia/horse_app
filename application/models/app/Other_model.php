<?php

class Other_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}	

	public function getNotesphotos($notes_id,&$errormessage)
	{
		$result = array();
		$this->db->select('id as photo_id,photo');
		$this->db->from('notes_photos');
		$this->db->where('notes_id',$notes_id);
		$this->db->where('active','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Photos not available";
		}
		return $result;
	}

	public function getHorseNotes($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('notes');
		$this->db->where('horse_id',$horse_id);
		$this->db->where('active','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}

	public function getAllNotes($user_id,&$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('notes');
		$this->db->where('user_id',$user_id);
		$this->db->where('active','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}

	public function getLastAppoinmentGroup()
	{
		$group = 1;
		$this->db->select_max('repeat_group');
		//$this->db->select('repeat_group');
		$this->db->from('horse_appoinements');
		$result = $this->db->get()->row_array();
		if(!empty($result))
		{
			$group = $result['repeat_group'] + 1;
		}
		return $group;
	}

	public function deleteAppoinmentFollowing($data,&$errormessage)
	{
		$result = 0;
		$this->db->where('repeat_group',$data['group']);
		$this->db->where('id >=',$data['record_id']);
		$result = $this->db->delete('horse_appoinements');
		if($result !== 1)
		{
			$errormessage = "Record not deleted.";
		}
		
		return $result;
	}
	
	public function getAllUsers(&$errormessage)
	{
		$result = array();
		$this->db->select('id,first_name,last_name,email,phone_no');
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('type','1');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}
	
	public function getSharedHorseUsers($horse_id,&$errormessage)
	{
		$result = array();
		$this->db->select('a.id,a.share_flag,b.first_name,b.last_name,b.email');
		$this->db->from('shared_horse a');
		$this->db->join('user b','a.user_id = b.id');
		$this->db->where('a.active','1');
		$this->db->where('a.horse_id',$horse_id);
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}
}