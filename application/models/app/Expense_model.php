<?php
//require(APPPATH.'models/Go_model.php');
class Expense_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function getBoardingExpense($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('boarding_expense');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getClinicExpense($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('clinic_expense');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getFeedExpense($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('feed_expense');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getHorsePurchase($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('horse_purchase');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getMembershipExpense($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('membership_expense');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getMiscExpense($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('misc_expense');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getTackExpense($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('tack_expense');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getTraileringExpense($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('trailering_expense');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}
}