<?php
//require(APPPATH.'models/Go_model.php');
class Income_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function getBoardingIncome($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('boarding_income');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getHorseSale($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('horse_sales');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getLessonIncome($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('lesson_income');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getMiscIncome($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('miscellaneous_income');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getTraileringIncome($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('trailering_income');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getTrainingIncome($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('training_income');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

}