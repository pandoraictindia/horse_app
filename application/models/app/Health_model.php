<?php
//require(APPPATH.'models/Go_model.php');
class Health_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function getDentistry($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('dentistry');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getDeworming($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('deworming');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getHoofcare($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('hoof_care');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getLegcare($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('leg_care');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getMiscHealth($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('misc_health');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getVaccinations($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('vaccinations');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getInjuries($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('injuries');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getTherapy($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('therapy');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getVetWork($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('vet_work');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getWeight($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('weights');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getMedications($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('medications');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getXrays($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('xrays');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getBloodWork($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('blood_work');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getChiro($horse_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('chiro');
		$this->db->where('active','1');
		$this->db->where('horse_id',$horse_id);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}
}