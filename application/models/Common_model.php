<?php
//require(APPPATH.'models/Go_model.php');
class Common_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function checkLoginSession($authcode,$userid,&$errormessage)
	{
		$result = 0;
		$query = $this->db->get_where('user_session',array('authcode' => $authcode, 'user_id'=>$userid));
        $sessiondata = $query->row_array();
       
		if(!empty($sessiondata))
		{
			$result = 1; 			
		}
		else
		{
			$errormessage = 'Authentication error. You need to login.'; 
		}
		return $result;
	}

	public function insertdata($data,$table,&$errormessage)
	{
		$result = 0;
		$result = $this->db->insert($table,$data);
		if($result == 1)
		{
			$result = $this->db->insert_id();
		}
		else
		{
			$errormessage = "Record not inserted.";
		}
		
		return $result;
	}

	public function updateRecord($data,$record_id,$table,&$errormessage)
	{
		$result = 0;
		$this->db->where('id',$record_id);
		$result = $this->db->update($table,$data);
		if($result !== 1)
		{
			$errormessage = "Record not updated.";
		}
		
		return $result;
	}

	public function deleteRecord($record_id,$table,&$errormessage)
	{
		$result = 0;
		$this->db->where('id',$record_id);
		$result = $this->db->update($table,array('active'=>'0'));
		if($result !== 1)
		{
			$errormessage = "Record not deleted.";
		}
		
		return $result;
	}

	

	public function deletephoto($photoid,&$errormessage)
	{
		$result = 0;
		$this->db->where('id',$photoid);
		$result = $this->db->delete('photos');
		if($result !== 1)
		{
			$errormessage = "Record not deleted.";
		}
		return $result;
	}

	public function deleteContactHorse($contact_id,&$errormessage)
	{
		$result = 0;
		$this->db->where('contact_id',$contact_id);
		$result = $this->db->delete('horse_contact');
		if($result !== 1)
		{
			$errormessage = "Record not deleted.";
		}
		return $result;
	}


	
}