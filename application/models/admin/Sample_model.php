<?php
//require(APPPATH.'models/Go_model.php');
class Sample_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function checkLogin($data,&$errormessage)
	{
		$result = array();
		$this->db->select('id,first_name,last_name,email,type');
		$this->db->from('user');
		$this->db->where('email',$data['email']);
		$this->db->where('password',$data['password']);
		$this->db->where('active','1');
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = 'Wrong email or password.';
		}
		
		return $result;
	}
	
}