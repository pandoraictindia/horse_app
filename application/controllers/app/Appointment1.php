<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Appointment extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/login_model');
	    $this->load->model('app/other_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}
	
	//Add Appoinments
    public function addappointment_post()
	{
        $data = array();
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$data['user_id'] = $this->post('user_id');
		$data['horse_id'] = $this->post('horse_id');
		$data['type'] = trim($this->post('appointment_type'));
		$data['date'] = trim($this->post('appointment_date'));
		$data['time'] = trim($this->post('appointment_time'));
		$data['contact_name'] = trim($this->post('contact_name'));		
		$data['is_repeat'] = trim($this->post('appointment_repeat'));
		$data['status'] = trim($this->post('status'));
		$data['selected_days'] = trim($this->post('selected_days'));
		$data['ends_on'] = trim($this->post('ends_on'));

		$data['reminder1'] = trim($this->post('reminder1'));
		$data['reminder2'] = trim($this->post('reminder2'));
		$data['reminder3'] = trim($this->post('reminder3'));

		switch ($data['type']) {
			case '1': // Veterinarian
					$data['treatment'] = trim($this->post('treatment'));
					$data['medicine'] = trim($this->post('medicine'));
					$data['note'] = trim($this->post('note'));
					break;

			case '2': // Saddler
					$data['note'] = trim($this->post('note'));
					break;

			case '3': // Dentist
					$data['general_treatment'] = trim($this->post('general_treatment'));
					$data['others'] = trim($this->post('others'));
					$data['others_desc'] = trim($this->post('others_desc'));
					$data['note'] = trim($this->post('note'));
					break;

			case '4': // Blacksmith
					$data['hoof_shoes'] = trim($this->post('hoof_shoes'));
					$data['others'] = trim($this->post('others'));
					$data['others_desc'] = trim($this->post('others_desc'));
					$data['note'] = trim($this->post('note'));
					break;

			case '5': // Anthelmintic Therapy
					$data['app_type'] = trim($this->post('app_type'));
					$data['vaccine'] = trim($this->post('vaccine'));
					break;

			case '6': // Vaccines
					$data['app_type'] = trim($this->post('app_type'));
					$data['medicine'] = trim($this->post('medicine'));
					break;

			case '7': // Others
					$data['app_type'] = trim($this->post('app_type'));
					$data['note'] = trim($this->post('note'));
					break;			
		}		
		
		// if($data['is_repeat'] == '3')
		// {
		//    $data['selected_days'] = date('d',strtotime($data['date'])); 
		// }
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{	
			$data['repeat_group'] = $this->other_model->getLastAppoinmentGroup();
			$current_date = date('Y-m-d', strtotime($data['date']));
			$end_date = date('Y-m-d', strtotime($data['ends_on']));

			switch ($data['is_repeat']) {
				case '0':
						// No Repeat						
						$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
						if($result > 0)
						{
							$this->uploadAppoinmentPhotos($base64image_arr,$result);
						}
						break;
				
				case '1':
						// Daily
						//get date array
						$Date = $this->getDatesFromRange($current_date, $end_date, 'P1D');
						for($i=0;$i<count($Date);$i++)
						{
							$data['date'] = $Date[$i];
							$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
							if($result > 0)
							{
								$this->uploadAppoinmentPhotos($base64image_arr,$result);
							}
						}
						break;

				case '2':
						// Weekly
						//get date array
				// 		$Date = $this->getDatesFromRange($current_date, $end_date, 'P1W');
				// 		for($i=0;$i<(count($Date) - 1);$i++)
				// 		{
				// 			$data['date'] = $Date[$i];
				// 			$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
				// 			if($result > 0)
				// 			{
				// 				$this->uploadAppoinmentPhotos($base64image_arr,$result);
				// 			}
				// 		}
				        $days_arr = explode(',', $data['selected_days']);
						$begin = new DateTime($current_date);
						$end = new DateTime($end_date);
						$interval = DateInterval::createFromDateString('1 day');
						$period = new DatePeriod($begin, $interval, $end);
						
						foreach ($period as $dt) 
						{
							$weekday = date('l', strtotime($dt->format("Y-m-d")));
							if(in_array($weekday, $days_arr))
							{
								$data['date'] = $dt->format("Y-m-d");
								$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
								if($result > 0)
								{
									$this->uploadAppoinmentPhotos($base64image_arr,$result);
								}
							}
						}	
						break;

				case '3':
						// Monthly
						//get date array
						$Date = $this->getDatesFromRange($current_date, $end_date, 'P1M');
						for($i=0;$i<(count($Date) - 1);$i++)
						{
							$data['date'] = $Date[$i];
							$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
							if($result > 0)
							{
								$this->uploadAppoinmentPhotos($base64image_arr,$result);
							}
						}
						break;

				case '4':
						// Every 3 days
						$Date = $this->getDatesFromRange($current_date, $end_date, 'P3D');
						for($i=0;$i<(count($Date));$i++)
						{
							$data['date'] = $Date[$i];
							$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
							if($result > 0)
							{
								$this->uploadAppoinmentPhotos($base64image_arr,$result);
							}
						}
				// 		$days_arr = explode(',', $data['selected_days']);
				// 		$begin = new DateTime($current_date);
				// 		$end = new DateTime($end_date);
				// 		$interval = DateInterval::createFromDateString('1 day');
				// 		$period = new DatePeriod($begin, $interval, $end);
						
				// 		foreach ($period as $dt) 
				// 		{
				// 			$weekday = date('l', strtotime($dt->format("Y-m-d")));
				// 			if(in_array($weekday, $days_arr))
				// 			{
				// 				$data['date'] = $dt->format("Y-m-d");
				// 				$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
				// 				if($result > 0)
				// 				{
				// 					$this->uploadAppoinmentPhotos($base64image_arr,$result);
				// 				}
				// 			}
				// 		}						
						break;

				case '5':
						// Every 6 weeks
						//get date array
						$Date = $this->getDatesFromRange($current_date, $end_date, 'P6W');
						for($i=0;$i<(count($Date) - 1);$i++)
						{
							$data['date'] = $Date[$i];
							$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
							if($result > 0)
							{
								$this->uploadAppoinmentPhotos($base64image_arr,$result);
							}
						}
						break;

				case '6':
						// Every 12 months
						//get date array
						$Date = $this->getDatesFromRange($current_date, $end_date, 'P1Y');
						for($i=0;$i<count($Date);$i++)
						{
							$data['date'] = $Date[$i];
							$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
							if($result > 0)
							{
								$this->uploadAppoinmentPhotos($base64image_arr,$result);
							}
						}
						break;			
			}

			//insert data
			//$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
			if($result > 0)
			{				
				$json = array("status"=>200,"message"=>'Appointment has been added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function uploadAppoinmentPhotos($image_arr, $appoinment_id)
	{
		if(!empty($image_arr) || $image_arr !== '')
		{
		    for($i=0;$i<count($image_arr);$i++)
		    {
		        $upload = PageBase::uploadImages($image_arr[$i],'images/appoinments/');
		        if($upload !== '')
        		{
        		    $data2['photo'] = $upload;
        		    $data2['appoinment_id'] = $appoinment_id;
					$res = $this->common_model->insertdata($data2, 'appoinments_photos',$errormessage);
        		}
		    }
    	}
	}

	function getDatesFromRange($start, $end, $Time_Interval, $format = 'Y-m-d') 
	{       
	    // Declare an empty array 
	    $array = array(); 
	      
	    // Variable that store the date interval 
	    // of period 1 day 
	    $interval = new DateInterval($Time_Interval); 
	  
	    $realEnd = new DateTime($end); 
	    $realEnd->add($interval); 
	  
	    $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
	  
	    // Use loop to store date into array 
	    foreach($period as $date) {                  
	        $array[] = $date->format($format);  
	    } 
	  
	    // Return the array elements 
	    return $array; 
	} 
	
    //get single user horse appoinments
    public function getsingleuserhorseappoinments_post()
    {
    		$user_id = $this->post('user_id');
    		$authcode = $this->post('authcode');	
    		$errormessage = '';
    
    		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
    		if($auth == 1)
    			{
    				$result = $this->login_model->getAllHorseAppoinments($user_id,$errormessage);
    				if(!empty($result))
    				{
    					for($i=0;$i<count($result);$i++)
    					{
    						$result[$i]['time'] = date('h:i', strtotime($result[$i]['time']));
    						$result[$i]['photos'] = $this->login_model->getappoinmentphotos($result[$i]['id'],$errormessage);
    					}
    					$json = array("status"=>200,"message"=>'success','records'=>$result);
    				}
    				else
    				{
    					$json = array("status"=>200,"message"=>$errormessage);
    				}
    			}
    			else
    			{
    				$json = array("status"=>400,"message"=>$errormessage);
    			}
    		
    		header('Access-Control-Allow-Origin: *');
    		header('Content-type: application/json');
    		echo json_encode($json);
    }

	//get horse appoinments by horse id
	public function gethorseappoinments_post()
	{
		$horse_id = $this->post('horse_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getHorseAppoinments($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{					
					$result[$i]['photos'] = $this->login_model->getappoinmentphotos($result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update  appoinments
	/*public function updateappointment_post()
	{
		$record_id = $this->post('record_id');
		$repeat_group = $this->post('repeat_group');
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$data['user_id'] = $this->post('user_id');
		$data['horse_id'] = $this->post('horse_id');
		$data['type'] = trim($this->post('appointment_type'));
		$data['date'] = trim($this->post('appointment_date'));
		$data['time'] = trim($this->post('appointment_time'));
		$data['contact_name'] = trim($this->post('contact_name'));		
		$data['is_repeat'] = trim($this->post('appointment_repeat'));
		$data['status'] = trim($this->post('status'));
		$data['selected_days'] = trim($this->post('selected_days'));
		$data['ends_on'] = trim($this->post('ends_on'));
		$data['keywork'] = trim($this->post('keywork'));
		$data['todo_text'] = trim($this->post('todo_text'));
		$data['result_medication'] = trim($this->post('result_medication'));

		$data['reminder1'] = trim($this->post('reminder1'));
		$data['reminder2'] = trim($this->post('reminder2'));
		$data['reminder3'] = trim($this->post('reminder3'));

		if ($data['type'] == '1') {
					$data['treatment'] = trim($this->post('treatment'));
					$data['medicine'] = trim($this->post('medicine'));
					$data['note'] = trim($this->post('note'));
		}

		if ($data['type'] == '2') {
			$data['note'] = trim($this->post('note'));
		}

		if ($data['type'] == '4') {
					$data['hoof_shoes'] = trim($this->post('hoof_shoes'));
					//$data['hoof_shoes_desc'] = trim($this->post('hoof_shoes_desc'));
					$data['others'] = trim($this->post('others'));
					$data['others_desc'] = trim($this->post('others_desc'));
						$data['note'] = trim($this->post('note'));
		}

		if ($data['type'] == '3') {
					$data['general_treatment'] = trim($this->post('general_treatment'));
					//$data['gen_treat_desc'] = trim($this->post('gen_treat_desc'));
					$data['others'] = trim($this->post('others'));
					$data['others_desc'] = trim($this->post('others_desc'));
						$data['note'] = trim($this->post('note'));
		}

		if ($data['type'] == '5') {
					$data['app_type'] = trim($this->post('app_type'));
					$data['vaccine'] = trim($this->post('vaccine'));
		}

		if ($data['type'] == '6') {
					$data['app_type'] = trim($this->post('app_type'));
					$data['medicine'] = trim($this->post('medicine'));
		}

		if ($data['type'] == '7') {
					$data['app_type'] = trim($this->post('app_type'));
					$data['note'] = trim($this->post('note'));
		}
		
		// if($data['is_repeat'] == '3')
		// {
		//    $data['selected_days'] = date('d',strtotime($data['date'])); 
		// }
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$delete['record_id'] = $record_id;	
			$delete['group'] = $repeat_group;
			//delete current records from record id
			$delete_records = $this->other_model->deleteAppoinmentFollowing($delete,$errormessage);

			if($delete_records == 1)
			{
				$data['repeat_group'] = $this->other_model->getLastAppoinmentGroup();
				$current_date = date('Y-m-d', strtotime($data['date']));
				$end_date = date('Y-m-d', strtotime($data['ends_on']));

				switch ($data['is_repeat']) {
					case '0':
							// No Repeat						
							$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
							if($result > 0)
							{
								$this->uploadAppoinmentPhotos($base64image_arr,$result);
							}
							break;
					
					case '1':
							// Daily
							//get date array
							$Date = $this->getDatesFromRange($current_date, $end_date, 'P1D');
							for($i=0;$i<count($Date);$i++)
							{
								$data['date'] = $Date[$i];
								$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
								if($result > 0)
								{
									$this->uploadAppoinmentPhotos($base64image_arr,$result);
								}
							}
							break;

					case '2':
							// Weekly
    				        $days_arr = explode(',', $data['selected_days']);
    						$begin = new DateTime($current_date);
    						$end = new DateTime($end_date);
    						$interval = DateInterval::createFromDateString('1 day');
    						$period = new DatePeriod($begin, $interval, $end);
    						
    						foreach ($period as $dt) 
    						{
    							$weekday = date('l', strtotime($dt->format("Y-m-d")));
    							if(in_array($weekday, $days_arr))
    							{
    								$data['date'] = $dt->format("Y-m-d");
    								$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
    								if($result > 0)
    								{
    									$this->uploadAppoinmentPhotos($base64image_arr,$result);
    								}
    							}
    						}	
							break;

					case '3':
							// Monthly
							//get date array
							$Date = $this->getDatesFromRange($current_date, $end_date, 'P1M');
							for($i=0;$i<(count($Date) - 1);$i++)
							{
								$data['date'] = $Date[$i];
								$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
								if($result > 0)
								{
									$this->uploadAppoinmentPhotos($base64image_arr,$result);
								}
							}
							break;

					case '4':
							// Every 3 days
							$Date = $this->getDatesFromRange($current_date, $end_date, 'P3D');
    						for($i=0;$i<(count($Date));$i++)
    						{
    							$data['date'] = $Date[$i];
    							$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
    							if($result > 0)
    							{
    								$this->uploadAppoinmentPhotos($base64image_arr,$result);
    							}
    						}
							break;

					case '5':
							// Every 6 weeks
							//get date array
							$Date = $this->getDatesFromRange($current_date, $end_date, 'P6W');
							for($i=0;$i<(count($Date) - 1);$i++)
							{
								$data['date'] = $Date[$i];
								$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
								if($result > 0)
								{
									$this->uploadAppoinmentPhotos($base64image_arr,$result);
								}
							}
							break;

					case '6':
							// Every 12 months
							//get date array
							$Date = $this->getDatesFromRange($current_date, $end_date, 'P1Y');
							for($i=0;$i<count($Date);$i++)
							{
								$data['date'] = $Date[$i];
								$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
								if($result > 0)
								{
									$this->uploadAppoinmentPhotos($base64image_arr,$result);
								}
							}
							break;			
				}
			}

			//update data
			//$result = $this->common_model->updateRecord($data,$record_id,'horse_appoinements',$errormessage);
			if($result > 0)
			{				
				$json = array("status"=>200,"message"=>'Appoinment updated succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}*/
	
	
	public function updateappointment_post()
	{
		$record_id = $this->post('record_id');
		//$repeat_group = $this->post('repeat_group');
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$data['user_id'] = $this->post('user_id');
		//$data['horse_id'] = $this->post('horse_id');
		//$type = trim($this->post('appointment_type'));
		$data['date'] = trim($this->post('appointment_date'));
		$data['time'] = trim($this->post('appointment_time'));
		$data['contact_name'] = trim($this->post('contact_name'));		
		//$data['is_repeat'] = trim($this->post('appointment_repeat'));
		$data['status'] = trim($this->post('status'));
		//$data['selected_days'] = trim($this->post('selected_days'));
		//$data['ends_on'] = trim($this->post('ends_on'));
		//$data['keywork'] = trim($this->post('keywork'));
		//$data['todo_text'] = trim($this->post('todo_text'));
		//$data['result_medication'] = trim($this->post('result_medication'));

		$data['reminder1'] = trim($this->post('reminder1'));
		$data['reminder2'] = trim($this->post('reminder2'));
		$data['reminder3'] = trim($this->post('reminder3'));
		
		$type = trim($this->post('appointment_type'));
		switch ($type) {
			case '1': // Veterinarian
					$data['treatment'] = trim($this->post('treatment'));
					$data['medicine'] = trim($this->post('medicine'));
					$data['note'] = trim($this->post('note'));
					break;

			case '2': // Saddler
					$data['note'] = trim($this->post('note'));
					break;

			case '3': // Dentist
					$data['general_treatment'] = trim($this->post('general_treatment'));
					$data['others'] = trim($this->post('others'));
					$data['others_desc'] = trim($this->post('others_desc'));
					$data['note'] = trim($this->post('note'));
					break;

			case '4': // Blacksmith
					$data['hoof_shoes'] = trim($this->post('hoof_shoes'));
					$data['others'] = trim($this->post('others'));
					$data['others_desc'] = trim($this->post('others_desc'));
					$data['note'] = trim($this->post('note'));
					break;

			case '5': // Anthelmintic Therapy
					$data['app_type'] = trim($this->post('app_type'));
					$data['vaccine'] = trim($this->post('vaccine'));
					break;

			case '6': // Vaccines
					$data['app_type'] = trim($this->post('app_type'));
					$data['medicine'] = trim($this->post('medicine'));
					break;

			case '7': // Others
					$data['app_type'] = trim($this->post('app_type'));
					$data['note'] = trim($this->post('note'));
					break;			
		}		

		
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//update data
			$result = $this->common_model->updateRecord($data,$record_id,'horse_appoinements',$errormessage);
			if($result == 1)
			{
			    $this->uploadAppoinmentPhotos($base64image_arr,$record_id);
				$json = array("status"=>200,"message"=>'Appoinment updated succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete appoinment
	public function deleteappoinment_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'horse_appoinements',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete appoinment with following
	public function deleteappoinmentfollowing_post()
	{
		$data['record_id'] = $this->post('record_id');	
		$data['group'] = $this->post('repeat_group');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->other_model->deleteAppoinmentFollowing($data,$errormessage);
			if($result == 1)
			{
			    //upload images
				$upload_img = [];
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				         $upload = PageBase::uploadImages($base64image_arr[$i],'images/appoinments/');
				        if($upload !== '')
	            		{
	            		    $data2['photo'] = $upload;
	            		    $data2['appoinment_id'] = $record_id;
							$res = $this->common_model->insertdata($data2, 'appoinments_photos',$errormessage);
	            		}
				    }
		    	}
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function addappoinmentnotification_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$appoinment_id = $this->post('appoinment_id');
		$data['before_text'] = trim($this->post('before_text'));
		$data['before_days'] = trim($this->post('before_days'));
		$data['day_or_hour'] = trim($this->post('day_or_hour'));
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->updateRecord($data,$appoinment_id,'horse_appoinements',$errormessage);
			if($result > 0)
			{				
				$json = array("status"=>200,"message"=>'notification added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function deleteappoinmentnotification_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$appoinment_id = $this->post('appoinment_id');
		$data['reminder1'] = NULL;
		$data['reminder2'] = NULL;
		$data['reminder3'] = NULL;
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->updateRecord($data,$appoinment_id,'horse_appoinements',$errormessage);
			if($result > 0)
			{				
				$json = array("status"=>200,"message"=>'notification deleted succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	public function deletespecificnotification_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$appoinment_id = $this->post('appoinment_id');
		$reminder = $this->post('reminder');
		$data['reminder'.$reminder] = NULL;
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$appoinment_id,'horse_appoinements',$errormessage);
			if($result > 0)
			{				
				$json = array("status"=>200,"message"=>'notification deleted succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}


}