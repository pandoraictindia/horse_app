<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Expense extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/expense_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//add boarding expense
	public function addboardingexpense_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/boarding/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'boarding_expense',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 10;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update boarding expense
	public function updateboardingexpense_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,'boarding_expense',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/boarding/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 10;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get boarding expense
	public function getboardingexpense_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getBoardingExpense($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(10,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete boarding expense
	public function deleteboardingexpense_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'boarding_expense',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add clinic expense
	public function addclinicexpense_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/clinic/');
			        if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'clinic_expense',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 11;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update clinic expense
	public function updateclinicexpense_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,'clinic_expense',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/clinic/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 11;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get clinic expense
	public function getclinicexpense_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getclinicExpense($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(11,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete clinic expense
	public function deleteclinicexpense_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'clinic_expense',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add feed expense
	public function addfeedexpense_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/feed/');
			        if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'feed_expense',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 12;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update feed expense
	public function updatefeedexpense_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,'feed_expense',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/feed/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 12;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get feed expense
	public function getfeedexpense_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getFeedExpense($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(12,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete feed expense
	public function deletefeedexpense_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'feed_expense',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add horse purchases expense
	public function addhorsepurchase_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['cost'] = trim($this->post('cost'));
		$data['purchase_from'] = trim($this->post('purchase_from'));
		$data['location'] = trim($this->post('location'));
		$data['contact'] = trim($this->post('contact'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/purchase/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'horse_purchase',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 15;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update horse purchases expense
	public function updatehorsepurchase_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['cost'] = trim($this->post('cost'));
		$data['purchase_from'] = trim($this->post('purchase_from'));
		$data['location'] = trim($this->post('location'));
		$data['contact'] = trim($this->post('contact'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,'horse_purchase',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/purchase/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 15;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>400,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>200,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get horsepurchase
	public function gethorsepurchase_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getHorsePurchase($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(15,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete horse purchase
	public function deletehorsepurchase_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'horse_purchase',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add membership expense
	public function addmembershipexpense_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['membership_with'] = trim($this->post('membership_with'));
		$data['cost'] = trim($this->post('cost'));
		$data['expiary_date'] = trim($this->post('expiary_date'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/membership/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'membership_expense',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 13;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update membership expense
	public function updatemembershipexpense_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['membership_with'] = trim($this->post('membership_with'));
		$data['cost'] = trim($this->post('cost'));
		$data['expiary_date'] = trim($this->post('expiary_date'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'membership_expense',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/membership/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 13;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get membership expense
	public function getmembershipexpense_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getMembershipExpense($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(13,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete membership expense
	public function deletemembershipexpense_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'membership_expense',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add misc expense
	public function addmiscexpense_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/miscellaneous/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'misc_expense',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 14;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update misc expense
	public function updatemiscexpense_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'misc_expense',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/miscellaneous/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 14;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get misc expense
	public function getmiscexpense_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getMiscExpense($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(14,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete misc expense
	public function deletemiscexpense_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'misc_expense',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add tack expense
	public function addtackexpense_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/tack/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'tack_expense',$errormessage);
			if($result > 0)
			{				
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 16;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}				
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update tack expense
	public function updatetackexpense_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'tack_expense',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/tack/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 16;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>400,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get tack expense
	public function gettackexpense_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getTackExpense($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(16,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete tack expense
	public function deletetackexpense_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'tack_expense',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add trailering expense
	public function addtraileringexpense_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['hauler_name'] = trim($this->post('hauler_name'));
		$data['t_from'] = trim($this->post('from'));
		$data['t_to'] = trim($this->post('to'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/trailering/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'trailering_expense',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 17;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update trailering expense
	public function updatetraileringexpense_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['hauler_name'] = trim($this->post('hauler_name'));
		$data['t_from'] = trim($this->post('from'));
		$data['t_to'] = trim($this->post('to'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'trailering_expense',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/expense/trailering/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 17;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get trailering expense
	public function gettraileringexpense_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->expense_model->getTraileringExpense($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(17,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete trailering expense
	public function deletetraileringexpense_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,
				'trailering_expense',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

}