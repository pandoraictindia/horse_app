<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Horse extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/login_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}	

	//add horse appoinments
	public function addhorseappointments_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$data['user_id'] = $this->post('user_id');
		$data['horse_id'] = $this->post('horse_id');
		$data['type'] = trim($this->post('type'));
		$data['date'] = trim($this->post('appointment_date'));
		$data['time'] = trim($this->post('appointment_time'));
		$data['contact_name'] = trim($this->post('contact_name'));		
		$data['repeating_appointment'] = trim($this->post('repeat_appoint'));
		$data['end_on'] = trim($this->post('end_onDate'));
		$data['days'] = trim($this->post('days'));
		$data['new_hoof_shoes'] = trim($this->post('new_hoof_shoes'));
		$data['notes'] = trim($this->post('notes'));
		$data['general_treatment'] = trim($this->post('general_treatment'));
		$data['vaccination_type'] = trim($this->post('vaccination_type'));
		$data['vaccine'] = trim($this->post('vaccine'));
		$data['other_appoinment_type'] = trim($this->post('other_appoinment_type'));		
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->insertdata($data,'horse_appoinements',$errormessage);
			if($result > 0)
			{
				//upload images
				$upload_img = [];
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				         $upload = PageBase::uploadImages($base64image_arr[$i],'images/appoinments/');
				        if($upload !== '')
	            		{
	            		    $data2['photo'] = $upload;
	            		    $data2['appoinment_id'] = $result;
							$res = $this->common_model->insertdata($data2, 'appoinments_photos',$errormessage);
	            		}
				    }
		    	}
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update horse appoinments
	public function updatehorseappointment_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$record_id = $this->post('record_id');
		$data['type'] = trim($this->post('type'));
		$data['date'] = trim($this->post('appointment_date'));
		$data['time'] = trim($this->post('appointment_time'));
		$data['contact_name'] = trim($this->post('contact_name'));
		$data['status'] = trim($this->post('appointment_status'));
		$data['keywork'] = trim($this->post('keywork'));
		$data['vaccination_type'] = trim($this->post('test'));
		$data['repeating_appointment'] = trim($this->post('repeat_appoint'));
		$data['end_on'] = trim($this->post('end_onDate'));
		$data['days'] = trim($this->post('days'));
		$data['new_hoof_shoes'] = trim($this->post('result_medicate'));		
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->updateRecord($data,$record_id,'horse_appoinements',$errormessage);
			if($result == 1)
			{
				//upload images
				$upload_img = [];
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/appoinments/');
				        if($upload !== '')
	            		{
	            		    $data2['photo'] = $upload;
							$res = $this->common_model->insertdata($data2, 'appoinments_photos',$errormessage);
	            		}
				    }
		    	}
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}	

	//get horse appoinments
	public function gethorseappoinments_post()
	{
		$horse_id = $this->post('horse_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getHorseAppoinments($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['time'] = date('h:i', strtotime($result[$i]['time']));
					$result[$i]['photos'] = $this->login_model->getappoinmentphotos($result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add other detail
	public function addotherdetail_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$horse_id = $this->post('horse_id');
		$data['live_no'] = trim($this->post('live_no'));
		$data['microchip_no'] = trim($this->post('microchip_no'));
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->updateRecord($data,$horse_id,'horses',$errormessage);
			if($result > 0)
			{				
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get other detail
	public function getotherdetail_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$horse_id = $this->post('horse_id');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->login_model->getOtherDetail($horse_id,$errormessage);
			if(!empty($result))
			{
				if($result['live_no'] == NULL)
				{
					$result['live_no'] = '';
				}
				if($result['microchip_no'] == NULL)
				{
					$result['microchip_no'] = '';
				}			
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add parents detail
	public function addparentsdetail_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$horse_id = $this->post('horse_id');
		$data['mother_name'] = trim($this->post('mother_name'));
		$data['father_name'] = trim($this->post('father_name'));
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->updateRecord($data,$horse_id,'horses',$errormessage);
			if($result > 0)
			{				
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get parents detail
	public function getparentsdetail_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$horse_id = $this->post('horse_id');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->login_model->getParentDetail($horse_id,$errormessage);
			if(!empty($result))
			{
				if($result['mother_name'] == NULL)
				{
					$result['mother_name'] = '';
				}
				if($result['father_name'] == NULL)
				{
					$result['father_name'] = '';
				}				
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add food detail
	public function addfooddetail_post()
	{
		$user_id = $this->post('user_id');
		$data['horse_id'] = $this->post('horse_id');
		$data['food_type'] = trim($this->post('food_type'));
		$data['food_amount'] = trim($this->post('food_amount'));
		$data['food_days'] = trim($this->post('food_days'));
		$data['food_time'] = trim($this->post('food_time'));
		//$data['start_date'] = trim($this->post('start_date'));
		//$data['end_date'] = trim($this->post('end_date'));
			
		$authcode = $this->post('authcode');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->insertdata($data,'food_detail',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get food detail
	public function getfooddetail_post()
	{
		$horse_id = $this->post('horse_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getFoodDetail($horse_id,$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete food detail
	public function deletefooddetail_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'food_detail',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//update food detail
	public function updatefooddetail_post()
	{
		$record_id = $this->post('record_id');
		$user_id = $this->post('user_id');
		$data['food_type'] = trim($this->post('food_type'));
		$data['food_amount'] = trim($this->post('food_amount'));
		$data['food_days'] = trim($this->post('food_days'));
		$data['food_time'] = trim($this->post('food_time'));
// 		$data['start_date'] = trim($this->post('start_date'));
// 		$data['end_date'] = trim($this->post('end_date'));
		$authcode = $this->post('authcode');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,'food_detail',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record updated succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>200,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add contacts
	public function addcontacts_post()
	{
		$user_id = $this->post('user_id');
		$data['user_id'] = $user_id;
		// $data['horse_id'] = $this->post('horse_id');
		$data['contact_name'] = trim($this->post('contact_name'));
		$data['contact_type'] = trim($this->post('contact_type'));
		$data['contact_number'] = trim($this->post('contact_number'));
		$data['email'] = trim($this->post('email'));
		$data['from_contact'] = trim($this->post('from_contact'));
		$data['address'] = trim($this->post('address'));
		$data['remark'] = trim($this->post('remark'));
		$authcode = $this->post('authcode');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$contact_id = $this->common_model->insertdata($data,'contacts',$errormessage);
			if($contact_id > 0)
			{
				//add horse of contacts
				$horse_ids = trim($this->post('horse_id'));
				if($horse_ids !== '') {
					$horse_id_arr = explode(',', $horse_ids);
					if(!empty($horse_id_arr)){
						for($i=0;$i<count($horse_id_arr);$i++) {
							$dt['contact_id'] = $contact_id;
							$dt['horse_id'] = $horse_id_arr[$i];
							$res = $this->common_model->insertdata($dt,'horse_contact',$errormessage);
						}
					}
				}
				$json = array("status"=>200,"message"=>'Record added succesfully.','contactid'=>$contact_id);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//update contacts
	public function updatecontact_post()
	{
		$user_id = $this->post('user_id');
		$contact_id = $this->post('contact_id');
		$contact_flag = $this->post('contact_flag');  //1-single horse contact, 2-all horse contact
		$data['contact_name'] = trim($this->post('contact_name'));
		$data['contact_type'] = trim($this->post('contact_type'));
		$data['contact_number'] = trim($this->post('contact_number'));
		$data['email'] = trim($this->post('contact_email'));
		$data['from_contact'] = trim($this->post('from_contact'));
		$data['address'] = trim($this->post('address'));
		$data['remark'] = trim($this->post('remark'));
		$authcode = $this->post('authcode');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{				
			//insert data
			$result = $this->common_model->updateRecord($data,$contact_id,'contacts',$errormessage);
			if($result > 0)
			{
				if($contact_flag == 2)
				{
					//remove old horse ids
					$delete_result = $this->common_model->deleteContactHorse($contact_id,$errormessage);

					//add horse of contacts
					if($delete_result == 1) {
						$horse_ids = trim($this->post('horse_id'));
						if($horse_ids !== '') {
							$horse_id_arr = explode(',', $horse_ids);
							if(!empty($horse_id_arr)){
								for($i=0;$i<count($horse_id_arr);$i++) {
									$dt['contact_id'] = $contact_id;
									$dt['horse_id'] = $horse_id_arr[$i];
									$res = $this->common_model->insertdata($dt,'horse_contact',$errormessage);
								}
							}
						}
					}
				}
				$json = array("status"=>200,"message"=>'Record updated succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get contacts
	public function getcontacts_post()
	{
		$horse_id = $this->post('horse_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getContacts($horse_id,$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//get contacts by user id
	public function getcontactsbyuserid_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getContactsByUser($user_id,$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete contacts
	public function deletecontacts_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'contacts',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get all horse appoinments
	public function getallhorseappoinments_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getAllHorseAppoinments($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->login_model->getappoinmentphotos($result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add horse photos
	public function addhorsephotos_post()
	{	
		$user_id = $this->post('user_id');	
		$horse_id  = $this->post('horse_id');		
		$base64image_arr = $this->post('image_arr');	
		$authcode = $this->post('authcode');

		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			if(!empty($base64image_arr) || $base64image_arr !== '')
			{
				$img_upload = 0;
				
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			        $upload = PageBase::uploadImages($base64image_arr[$i],'images/horse_photo/');
			        if($upload !== '')
            		{
            		    
            		    $data['horse_id'] = $horse_id;
            		    $data['photo'] = $upload;
						$res = $this->common_model->insertdata($data,'horse_photos',$errormessage);
						if($res == 0)
					    {
					        $img_upload = 1;
					    }
            		}
			    }
	    	}

			if($img_upload == 1)
			{
			    $json = array("status"=>400,"message"=>'Something wrong in uploading image. Try again.');
			}
			else
			{
				$json = array("status"=>200,"message"=>'Photos uploaded succesfully.');
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get horse detail from horse id
	public function gethorsedetail_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$horse_id = $this->post('horse_id');
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getHorseDetail($horse_id,$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','detail'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get horse photos
	public function gethorsephotos_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$horse_id = $this->post('horse_id');
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getHorsephotos($horse_id,$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','photos'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete horse
	public function deletehorse_post()
	{
		$horse_id = $this->post('horse_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($horse_id,'horses',$errormessage);
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//update horse profile photo
	public function updatehorseprofilephoto_post()
	{
		$horse_id = $this->post('horse_id');
		$user_id = $this->post('user_id');
		$photo_name = trim($this->post('photo_name'));
		$authcode = $this->post('authcode');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$data['profile_photo'] = $photo_name;
			$result = $this->common_model->updateRecord($data,$horse_id,'horses',$errormessage);
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Profile photo updated succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//add horse photos caption
	public function addhorsephotocaption_post()
	{	
		$user_id = $this->post('user_id');	
		$photo_id  = $this->post('photo_id');		
		$data['caption'] = $this->post('caption');	
		$authcode = $this->post('authcode');

		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$photo_id,'horse_photos',$errormessage);
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Caption added succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//get horse locations
	public function gethorselocation_post()
	{
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$horse_id = $this->post('horse_id');
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getHorseLocation($horse_id,$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','location'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

}