<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Health extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/health_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//add dentistry
	public function adddentistry_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['dentist_name'] = trim($this->post('dentist_name'));
		$data['d_procedure'] = trim($this->post('procedure'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/dentistry/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'dentistry',$errormessage);

			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 25;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update dentistry
	public function updatedentistry_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['dentist_name'] = trim($this->post('dentist_name'));
		$data['d_procedure'] = trim($this->post('procedure'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'dentistry',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/dentistry/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 25;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get dentistry
	public function getdentistry_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getDentistry($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(25,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete dentistry
	public function deletedentistry_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'dentistry',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add deworming
	public function addeworming_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['brand'] = trim($this->post('brand'));
		$data['dosage'] = trim($this->post('dosage'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/deworming/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'deworming',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 26;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update deworming
	public function updatedeworming_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['brand'] = trim($this->post('brand'));
		$data['dosage'] = trim($this->post('dosage'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'deworming',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/deworming/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 26;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get deworming
	public function getdeworming_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getDeworming($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(26,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete deworming
	public function deletedeworming_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'deworming',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add hoof care
	public function addhoofcare_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['trimmed'] = trim($this->post('trimmed'));
		$data['shoe_reset'] = trim($this->post('shoe_reset'));
		$data['new_shoes'] = trim($this->post('new_shoes'));
		$data['farrier_name'] = trim($this->post('farrier_name'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/hoof-care/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'hoof_care',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 27;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update hoof care
	public function updatehoofcare_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['trimmed'] = trim($this->post('trimmed'));
		$data['shoe_reset'] = trim($this->post('shoe_reset'));
		$data['new_shoes'] = trim($this->post('new_shoes'));
		$data['farrier_name'] = trim($this->post('farrier_name'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'hoof_care',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/hoof-care/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 27;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get hoof care
	public function gethoofcare_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getHoofcare($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(27,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete hoof care
	public function deletehoofcare_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'hoof_care',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add leg care
	public function addlegcare_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['treatment'] = trim($this->post('treatment'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/leg-care/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'leg_care',$errormessage);

			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 28;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update leg care
	public function updatelegcare_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$data['treatment'] = trim($this->post('treatment'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'leg_care',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/leg-care/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 28;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get leg care
	public function getlegcare_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getLegcare($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(28,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete leg care
	public function deletelegcare_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'leg_care',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add misc health
	public function addmischealth_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['health_condition'] = trim($this->post('condition'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/misc/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'misc_health',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 29;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update misc health
	public function updatemischealth_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['health_condition'] = trim($this->post('condition'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'misc_health',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/misc/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 29;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get misc health
	public function getmischealth_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getMiscHealth($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(29,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete misc health
	public function deletemischealth_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'misc_health',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add vaccinations
	public function addvaccinations_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['dosage'] = trim($this->post('dosage'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/vaccination/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'vaccinations',$errormessage);

			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 30;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update vaccinations
	public function updatevaccinations_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['dosage'] = trim($this->post('dosage'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('due_next'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'vaccinations',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/vaccination/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 30;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get vaccinations
	public function getvaccinations_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getVaccinations($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(30,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete vaccinations
	public function deletevaccinations_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'vaccinations',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add injuries
	public function addinjuries_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['status'] = trim($this->post('status'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/injuries');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'injuries',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 31;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update injuries
	public function updateinjuries_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['status'] = trim($this->post('status'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'injuries',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/injuries/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 31;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get injuries
	public function getinjuries_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getInjuries($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(31,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete injuries
	public function deleteinjuries_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'injuries',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add therapy
	public function addtherapy_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/therapy/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'therapy',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 32;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update therapy
	public function updatetherapy_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'therapy',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/therapy/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 32;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get therapy
	public function gettherapy_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getTherapy($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(32,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete therapy
	public function deletetherapy_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'therapy',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add vet work
	public function addvetwork_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/vetwork/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'vet_work',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 33;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update vet work
	public function updatevetwork_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'vet_work',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/vetwork/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 33;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get vet work
	public function getvetwork_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getVetWork($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(33,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete vet work
	public function deletevetwork_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'vet_work',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add weight
	public function addweight_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['weight'] = trim($this->post('weight'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/weight/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'weights',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 34;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update weight
	public function updateweight_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['weight'] = trim($this->post('weight'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'weights',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/weight/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 34;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get weight
	public function getweight_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getWeight($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(34,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete weights
	public function deleteweight_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'weights',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add medications
	public function addmedications_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['treating_for'] = trim($this->post('treating_for'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/medication/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'medications',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 35;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update medications
	public function updatemedications_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['treating_for'] = trim($this->post('treating_for'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'medications',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/medication/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 35;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get medications
	public function getmedications_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getMedications($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(35,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete medications
	public function deletemedications_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'medications',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add xray
	public function addxray_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/xray/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'xrays',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 36;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update xray
	public function updatexray_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['type'] = trim($this->post('type'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'xrays',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/xray/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 36;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get xray
	public function getxray_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getXrays($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(36,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete xray
	public function deletexray_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'xrays',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add bloodwork
	public function addbloodwork_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['test_for'] = trim($this->post('test_for'));
		$data['result'] = trim($this->post('results'));
		$data['cost'] = trim($this->post('cost'));
		$data['notes'] = trim($this->post('notes'));
		$data['next_due'] = trim($this->post('next_due'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/blood-work/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'blood_work',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 37;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update bloodwork
	public function updatebloodwork_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['test_for'] = trim($this->post('test_for'));
		$data['result'] = trim($this->post('results'));
		$data['cost'] = trim($this->post('cost'));
		$data['next_due'] = trim($this->post('next_due'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'blood_work',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/blood-work/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 37;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get bloodwork
	public function getbloodwork_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getBloodWork($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(37,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete bloodwork
	public function deletebloodwork_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'blood_work',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add chiro
	public function addchiro_post()
	{
		$data['horse_id'] = $this->post('apply_to');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/chiro/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'chiro',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 38;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update chiro
	public function updatechiro_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'chiro',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/health/chiro/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 38;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>400,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>200,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get chiro
	public function getchiro_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->health_model->getChiro($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(38,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete chiro
	public function deletechiro_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'chiro',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

}