<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Income extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/income_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//add boarding income
	public function addboardingincome_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/boarding/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'boarding_income',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 18;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update boarding income
	public function updateboardingincome_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'boarding_income',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/boarding/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 18;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get boarding income
	public function getboardingincome_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->income_model->getBoardingIncome($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(18,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete boarding income
	public function deleteboardingincome_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'boarding_income',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add Horse Sales income
	public function addhorsesale_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['contact_info'] = trim($this->post('contact_info'));
		$data['location_info'] = trim($this->post('location_info'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/sales/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'horse_sales',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 21;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update Horse Sales income
	public function updatehorsesale_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('date'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['contact_info'] = trim($this->post('contact_info'));
		$data['location_info'] = trim($this->post('location_info'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'horse_sales',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/sales/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 21;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get horse sale income
	public function gethorsesale_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->income_model->getHorseSale($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(21,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete horse sale income
	public function deletehorsesale_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'horse_sales',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add lesson income
	public function addlessonincome_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/lessons/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'lesson_income',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 19;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update lesson income
	public function updatelessonincome_post()
	{
		$record_id = $this->post('record_id');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'lesson_income',$errormessage);
			if($result > 0)
			{				
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/lessons/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 19;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get lesson income
	public function getlessonincome_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->income_model->getLessonIncome($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(19,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete lesson income
	public function deletelessonincome_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'lesson_income',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add misc income
	public function addmiscincome_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			        $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/miscellaneus/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'miscellaneous_income',$errormessage);
			if($result > 0)
			{				
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 20;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update misc income
	public function updatemiscincome_post()
	{
		$record_id = $this->post('record_id');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'miscellaneous_income',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/miscellaneus/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 2;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get misc income
	public function getmiscincome_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->income_model->getMiscIncome($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(20,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete misc income
	public function deletemiscincome_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,
				'miscellaneous_income',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add trailering income
	public function addtraileringincome_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['trailered_from'] = trim($this->post('trailered_from'));
		$data['trailered_to'] = trim($this->post('trailered_to'));
		$data['distance'] = trim($this->post('distance'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/trailering/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'trailering_income',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 22;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>400,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>200,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update trailering income
	public function updatetraileringincome_post()
	{
		$record_id = $this->post('record_id');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['client_name'] = trim($this->post('client_name'));
		$data['trailered_from'] = trim($this->post('trailered_from'));
		$data['trailered_to'] = trim($this->post('trailered_to'));
		$data['distance'] = trim($this->post('distance'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'trailering_income',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/trailering/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 22;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get trailering income
	public function gettraileringincome_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->income_model->getTraileringIncome($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(22,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete trailering income
	public function deletetraileringincome_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,
				'trailering_income',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add training income
	public function addtrainingincome_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['date'] = trim($this->post('date'));		
		$data['client_name'] = trim($this->post('client_name'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			         $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/training/');
			         if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}	
			//insert data
			$result = $this->common_model->insertdata($data,'training_income',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['record_id'] = $result;
				    $img['type_id'] = 23;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update training income
	public function updatetrainingincome_post()
	{
		$record_id = $this->post('record_id');
		$user_id = $this->post('user_id');
		$data['date'] = trim($this->post('date'));		
		$data['client_name'] = trim($this->post('client_name'));
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['amount'] = trim($this->post('amount'));
		$data['invoice'] = trim($this->post('invoice'));
		$data['notes'] = trim($this->post('notes'));
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'training_income',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/income/training/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['record_id'] = $record_id;
						    $img['type_id'] = 23;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>200,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get training income
	public function gettrainingincome_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->income_model->getTrainingIncome($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->common_model->getphotos(23,$result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete training income
	public function deletetrainingincome_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,
				'trailering_income',$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

}