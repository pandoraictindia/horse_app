<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Other extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/other_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//add notes
	public function addnotes_post()
	{
		$data['user_id'] = $this->post('user_id');
		$data['horse_id'] = $this->post('horse_id');
		$data['date'] = trim($this->post('date'));
		$data['description'] = trim($this->post('description'));
		$base64image_arr = $this->post('image_arr');
		$authcode = $this->post('authcode');
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			//upload images
			$upload_img = [];
		    if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    for($i=0;$i<count($base64image_arr);$i++)
			    {
			        $upload = PageBase::uploadImages($base64image_arr[$i],'images/notes/');
			        if($upload !== '')
            		{
            		    array_push($upload_img,$upload);
            		}
			    }
	    	}
	    	//insert data	
			$result = $this->common_model->insertdata($data,'notes',$errormessage);
			if($result > 0)
			{
				//insert image upload data
				for($j=0;$j<count($upload_img);$j++)
				{
				    $upload_entry = 0;
				    $img['notes_id'] = $result;
				    $img['photo'] = $upload_img[$j];
				    
				    $res = $this->common_model->insertdata($img,'notes_photos',$errormessage);
				    if($res == 0)
				    {
				        $upload_entry = 1;
				    }
				}
				if($upload_entry == 1)
				{
				    $json = array("status"=>400,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record added succesfully.');
				}				
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>200,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update notes
	public function updatenotes_post()
	{
		$record_id = $this->post('record_id');
		$data['date'] = trim($this->post('journal_date'));
		$data['description'] = trim($this->post('description'));
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');
		$base64image_arr = $this->post('image_arr');
		$errormessage='';
		
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data,$record_id,
				'notes',$errormessage);
			if($result > 0)
			{
				$img_upload = 0;
				//upload new photos
			    if(!empty($base64image_arr) || $base64image_arr !== '')
				{						
				    for($i=0;$i<count($base64image_arr);$i++)
				    {
				        $upload = PageBase::uploadImages($base64image_arr[$i],'images/notes/');
				        if($upload !== '')
	            		{		            		  
            		       	$upload_entry = 0;
						    $img['notes_id'] = $record_id;
						    $img['photo'] = $upload;
						    
						    $res = $this->common_model->insertdata($img,'notes_photos',$errormessage);
						    if($res == 0)
						    {
						        $img_upload = 1;
						    }
	            		}
				    }
		    	}
		    	if($img_upload == 1)
				{
				    $json = array("status"=>400,"message"=>'Something wrong in uploading image. Try again.');
				}
				else
				{
					$json = array("status"=>200,"message"=>'Record updated succesfully.');
				}
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>200,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get notes
	public function gethorsenotes_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->other_model->getHorseNotes($horse_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->other_model->getNotesphotos($result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete notes
	public function deletenotes_post()
	{
		$record_id = $this->post('record_id');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($record_id,'notes',$errormessage);
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete horse image
	public function deletehorseimage_post()
	{
		$photoid = $this->post('photoid');	
		$image_name = $this->post('image_name');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{			
			$result = $this->common_model->deleteRecord($photoid,'horse_photos',$errormessage);
			if($result == 1)
			{
				if (file_exists($image_name))
				{
				    unlink($image_name);
				}
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get all notes
	public function getallnotes_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->other_model->getAllNotes($user_id,$errormessage);
			if(!empty($result))
			{
				for($i=0;$i<count($result);$i++)
				{
					$result[$i]['photos'] = $this->other_model->getNotesphotos($result[$i]['id'],$errormessage);
				}
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete notes image
	public function deletenotesimage_post()
	{
		$photoid = $this->post('photoid');	
		$image_name = $this->post('image_name');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{			
			$result = $this->common_model->deleteRecord($photoid,'notes_photos',$errormessage);
			if($result == 1)
			{
				if (file_exists($image_name))
				{
				    unlink($image_name);
				}
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete appoinments image
	public function deleteappoinmentimage_post()
	{
		$photoid = $this->post('photoid');	
		$image_name = $this->post('image_name');	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{			
			$result = $this->common_model->deleteRecord($photoid,'appoinments_photos',$errormessage);
			if($result == 1)
			{
				if (file_exists($image_name))
				{
				    unlink($image_name);
				}
				$json = array("status"=>200,"message"=>'Record deleted successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//get all user emails
	public function getallusers_post()
	{	
		$user_id = $this->post('user_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->other_model->getAllUsers($errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//share horse
	public function sharehorse_post()
	{	
		$user_id = $this->post('user_id');
		$data['user_id'] = $this->post('shared_user_id');
		$data['horse_id'] = $this->post('horse_id');
		$data['share_flag'] = $this->post('share_flag');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'shared_horse',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Horse shared successfully');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//get shared users of horse
	public function getsharedusersofhorse_post()
	{	
		$user_id = $this->post('user_id');
		$horse_id = $this->post('horse_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->other_model->getSharedHorseUsers($horse_id,$errormessage);
			if(!empty($result))
			{
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//delete share horse
	public function deletesharehorse_post()
	{	
		$user_id = $this->post('user_id');
		$shared_id = $this->post('shared_id');
		$authcode = $this->post('authcode');	
		$errormessage = '';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->deleteRecord($shared_id,'shared_horse',$errormessage);
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Share removed successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}


}