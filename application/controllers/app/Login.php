<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Login extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/login_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//app Login
	public function index_post()
	{
		$data['email'] = trim($this->post('email'));
		$data['password'] = md5(trim($this->post('password')));
		$errormessage='';

		$result = $this->login_model->checkUserLogin($data,$errormessage);
		if(!empty($result))
		{
			$data1['user_id'] = $result['id'];
			$data1['authcode'] = md5(uniqid());
			$auth = $this->login_model->addUserSession($data1,$errormessage);
			if($auth == 1)
			{
				$result['authcode'] = $data1['authcode'];
				$json = array("status"=>200,"message"=>'success','user_data'=>$result);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//forget password
	public function forgetpassword_post()
	{
		$email = trim($this->post('email'));
		$errormessage='';
		//check email exists
		$exists = $this->login_model->checkEmail($email,$errormessage);
		if(!empty($exists))
		{  
			//add forget pass token
			$token = uniqid();
			$addtoken = $this->login_model->addForgetPassToken($email,$token,$errormessage);
			if($addtoken == 1)
			{
				$html_data['detail']['username'] = $exists['first_name']. ' '.$exists['last_name'];
				$html_data['detail']['reset_pass_url'] = PageBase::$reset_pass_url.'?token='.$token;
				$data['to'] = $email;
				$data['subject'] = 'Forgot password';
				$data['title'] = 'Forgot password';
				$data['html'] = $this->load->view('forgot_password',$html_data, true);
				// $data['html'] = '<div>Hello '.$name.',<br><br>Reset your password.<a href="'.PageBase::$reset_pass_url.'?token='.$token.'">Click here</a></div>';
				PageBase::sendEmail($data);
			    $json = array("status"=>200,"message"=>'Check Email.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//Change Password
	public function changepassword_post()
	{
		$data['oldpass'] = trim($this->post('oldpass'));
		$data['newpass'] = trim($this->post('newpass'));
		$data['confirm_pass'] = trim($this->post('confirm_pass'));
		$data['user_id'] = $this->post('user_id');
		$authcode = $this->post('authcode');
		$errormessage = '';

		if($data['newpass'] == $data['confirm_pass'])
		{
			$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
			if($auth == 1)
			{
				$result = $this->login_model->changePassword($data,$errormessage);			
				if($result == 1)
				{
					$json = array("status"=>200,"message"=>'Password changed successfully.',);
				}
				else
				{
					$json = array("status"=>400,"message"=>$errormessage);
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>'New password and confirm password not match.');
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}	

	public function registeruser_post()
	{
		$data['first_name'] = trim($this->post('first_name'));
		$data['last_name'] = trim($this->post('last_name'));
		$data['email'] = trim($this->post('email'));
		$data['password'] = md5(trim($this->post('password')));
		$errormessage='';
		
		$email_exists = $this->login_model->checkEmail($data['email'],$errormessage);
		if(empty($email_exists))
		{
			$result = $this->common_model->insertdata($data,'user',$errormessage);
			if($result > 0)
			{
				$data1['user_id'] = $result;
				$data1['authcode'] = md5(uniqid());
				$auth = $this->login_model->addUserSession($data1,$errormessage);
				if($auth == 1)
				{
					$userdata = $this->login_model->getUserDetail($result,$errormessage);
					$userdata['authcode'] = $data1['authcode'];
					$json = array("status"=>200,"message"=>'success','user_data'=>$userdata);
				}
				else
				{
					$json = array("status"=>200,"message"=>'Registration success.');
				}				
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>200,"message"=>'Email already exists.');
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}	

	//add horse
	public function addhorse_post()
	{	
		$data['user_id'] = $this->post('user_id');	
		$data['horse_name'] = trim($this->post('horse_name'));
		$data['remark'] = trim($this->post('remark'));		
		$data['birthdate'] = trim($this->post('birthdate'));
		$data['mother_name'] = trim($this->post('mother_name'));
		$data['father_name'] = trim($this->post('father_name'));
		$data['live_no'] = trim($this->post('live_no'));
		$data['microchip_no'] = trim($this->post('microchip_no'));
		$base64image_arr = $this->post('image_arr');	
		$authcode = $this->post('authcode');
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
		    $upload = '';
			if(!empty($base64image_arr) && $base64image_arr !== '')
			{
			    
			    $upload = PageBase::uploadImages($base64image_arr,'images/horse_photo/');
		        if($upload !== '')
        		{
        		    $data['profile_photo'] = $upload;
        		}
	    	}

			$add_horse = $this->common_model->insertdata($data,'horses',$errormessage);
			if($add_horse > 0)
			{
			    
			    if($upload !== '')
			    {
    			    $data1['horse_id'] = $add_horse;
        		    $data1['photo'] = $upload;
    				$res = $this->common_model->insertdata($data1,'horse_photos',$errormessage);
			    }
				$json = array("status"=>200,"message"=>'Horse added successfully.','horse_id'=>$add_horse);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get my horses
	public function getmyhorse_post()
	{		
		$user_id = (Int)$this->post('user_id');
		$authcode = $this->post('authcode');
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->login_model->getMyHorse($user_id,$errormessage);
			if(!empty($result))
			{						
				$json = array("status"=>200,"message"=>'success','records'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add subscribe status
	public function addsubscribestatus_post()
	{	
		$user_id = $this->post('user_id');	
		$authcode = $this->post('authcode');

		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$data['subscribe_status'] = '1';
			$add_horse = $this->common_model->updateRecord($data,$user_id,'user',$errormessage);
			if($add_horse > 0)
			{				
				$json = array("status"=>200,"message"=>'Record added successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//update basic detail of horse
	public function updatebasicdetail_post()
	{	
		$user_id = $this->post('user_id');	
		$authcode = $this->post('authcode');
		$horseid = $this->post('horseid');
		$data['horse_name'] = $this->post('horse_name');
		$data['remark'] = $this->post('remark');
		$data['birthdate'] = $this->post('birthdate');
		$data['mother_name'] = trim($this->post('mother_name'));
		$data['father_name'] = trim($this->post('father_name'));
		$data['live_no'] = trim($this->post('live_no'));
		$data['microchip_no'] = trim($this->post('microchip_no'));
		$base64image_arr = $this->post('image_arr');
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			if(!empty($base64image_arr) || $base64image_arr !== '')
			{
			    $upload = PageBase::uploadImages($base64image_arr,'images/horse_photo/');
		        if($upload !== '')
        		{
        		    $data['profile_photo'] = $upload;
        		}
	    	}

			$update_horse = $this->common_model->updateRecord($data,$horseid,'horses',$errormessage);
			if($update_horse == 1)
			{				
				$json = array("status"=>200,"message"=>'Record updated successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}	

	//update user detail
	public function updateuserdetail_post()
	{	
		$user_id = $this->post('user_id');	
		$authcode = $this->post('authcode');
		$data['first_name'] = trim($this->post('first_name'));
		$data['last_name'] = trim($this->post('last_name'));
		$data['phone_no'] = trim($this->post('phone_no'));
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$update_user = $this->common_model->updateRecord($data,$user_id,'user',$errormessage);
			if($update_user == 1)
			{
				$userdata = $this->login_model->getUserDetail($user_id,$errormessage);			
				$json = array("status"=>200,"message"=>'Record updated successfully.','user_detail'=>$userdata);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//add location
	public function addlocation_post()
	{	
		$data['user_id'] = $this->post('user_id');	
		$data['horse_id'] = trim($this->post('horse_id'));	
		$data['lattitude'] = trim($this->post('lattitude'));
		$data['longitude'] = trim($this->post('longitude'));
		$authcode = $this->post('authcode');

		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			$locations = $this->common_model->insertdata($data,'locations',$errormessage);
			if($locations > 0)
			{
				$json = array("status"=>200,"message"=>'Location has been added successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	//update location
	public function updatelocation_post()
	{	
		$data['user_id'] = $this->post('user_id');		
		$data['lattitude'] = trim($this->post('lattitude'));
		$data['longitude'] = trim($this->post('longitude'));
		$authcode = $this->post('authcode');
		$record_id = $this->post('location_id');

		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->updateRecord($data, $record_id, 'locations',$errormessage);
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Location has been updated successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
}