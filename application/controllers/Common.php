<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Common extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}
	
}