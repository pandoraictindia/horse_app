<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Sample extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('admin/sample_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//admin login
	public function index_post()
	{
		$data['email'] = trim($this->post('email'));
		$data['password'] = md5(trim($this->post('password')));
		$errormessage='';

		$islogin = $this->sample_model->checkLogin($data,$errormessage);
		if(!empty($islogin))
		{
			if($islogin['type'] == '2')
			{
				$json = array("status"=>200,"message"=>'success','logindata'=>$islogin);
			}
			else
			{
				$json = array("status"=>400,"message"=>'You are not authorized user');
			}
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}


		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//create representative
	public function addrepresentative_post()
	{
		$data['name'] = trim($this->post('re_name'));
		$data['contact_no'] = trim($this->post('contact'));
		$data['type'] = trim($this->post('type'));
		$data['email'] = trim($this->post('email'));
		$editid = $this->post('editid');
		$data['password'] = PageBase::generaterandomno(6);		
		$errormessage='';
		if($editid == '')
		{
			//check email exists
			$email_exists = $this->common_model->checkEmailExists($data['email'],$errormessage);
			if($email_exists !== 1)
			{
				//add representative
				$result = $this->common_model->insertdata($data,'representative',$errormessage);
				if($result > 0)
				{
					$data['to'] = $data['email'];
					$data['subject'] = 'Registration';
					$data['html'] = '<div>Hello '.$data['name'].',<br>Check your login credentials: <br> Username: '.$data['email'].'<br>Password: '.$data['password'].'</div>';
					PageBase::sendEmail($data);
					$json = array("status"=>200,"message"=>'Record inserted succesfully.');
				}
				else
				{
					$json = array("status"=>400,"message"=>$errormessage);
				}
			}
			else
			{
				$json = array("status"=>400,"message"=>'Email already exists.');
			}
		}
		else
		{
			$result = $this->sample_model->updateRepresentative($data,$editid,$errormessage);
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Record updated succesfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get representatives
	public function getrepresentative_post()
	{
		$data['begin'] = trim($this->post('begin'));
		$errormessage='';
		$result = $this->sample_model->getRepresentative($data,$errormessage);
		if(!empty($result))
		{
			$data['begin'] = '';
			$total = count($this->sample_model->getRepresentative($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','rep_list'=>$result,'totalcount'=>$total);
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete representative
	public function deleterepresentative_post()
	{
		$record_id = $this->post('delete_id');
		$errormessage='';

		$result = $this->sample_model->deleteRepresentative($record_id,$errormessage);
		if($result == 1)
		{
			$json = array("status"=>200,"message"=>'Record deleted successfully.');
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get customer
	public function getcustomers_post()
	{
		$data['begin'] = trim($this->post('begin'));			
		$errormessage='';
		$result = $this->common_model->getCustomers($data,$errormessage);
		if(!empty($result))
		{
			$total = count($result = $this->sample_model->getCustomers($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','cust_list'=>$result, 'totalcount'=> $total);
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete customer
	public function deletecustomer_post()
	{
		$record_id = $this->post('delete_id');
		$errormessage='';

		$result = $this->sample_model->deleteCustomer($record_id,$errormessage);
		if($result == 1)
		{
			$json = array("status"=>200,"message"=>'Record deleted successfully.');
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get visit report
	public function getvisitreport_post()
	{
		$data['begin'] = $this->post('begin');
		$data['cust_name'] = $this->post('cust_name');
		$data['re_name'] = $this->post('re_name');
		$data['visit_status'] = $this->post('status');
		$data['start_date'] = $this->post('from_date');
		$data['end_date'] = $this->post('to_date');
		$errormessage='';

		$result = $this->sample_model->getVisitReport($data,$errormessage);
		if(!empty($result))
		{
			$total = count($result = $this->sample_model->getVisitReport($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','visits'=>$result,'totalcount'=>$total);
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get dashboard detail
	public function getdashboarddetail_get()
	{		
		$errormessage='';
		$result = $this->sample_model->getDashboardDetail($errormessage);
		if(!empty($result))
		{
			$json = array("status"=>200,"message"=>'success','detail'=>$result);
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get call notifications
	public function getcalls_post()
	{	
		$data['begin'] = $this->post('begin');	
		$errormessage = '';

		$result = $this->sample_model->getCalls($data,$errormessage);
		if(!empty($result))
		{
			$data['begin'] = '';
			$total = count($this->sample_model->getCalls($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','calls' => $result,'totalcount'=>$total);
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add call
	public function addcall_post()
	{
		$data['cust_id'] = $this->post('cust_id');
		$data['representative_id'] = $this->post('reps_id');
		$data['note'] = trim($this->post('note'));
		$data['visit_date'] = $this->post('visitdate');
		$editid = $this->post('editid');
		$errormessage='';

		if($editid == '')
		{
			$result = $this->common_model->insertdata($data,'service_call',$errormessage);
			if($result > 0)
			{
				$fcm = $this->sample_model->getDeviceId($$data['representative_id'],$errormessage);
				if(!empty($fcm))
				{
					$message = "New call is schedule.";
					PageBase::push_notification_android($fcm['fcm_token'],$message);
				}
				$json = array("status"=>200,"message"=>'Record added successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$result = $this->sample_model->updateCall($data,$editid,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record updated successfully.');
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//delete call
	public function deletecall_post()
	{
		$record_id = $this->post('delete_id');
		$errormessage='';

		$result = $this->sample_model->deleteCall($record_id,$errormessage);
		if($result == 1)
		{
			$json = array("status"=>200,"message"=>'Record deleted successfully.');
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
}