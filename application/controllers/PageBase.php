<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

date_default_timezone_set('asia/kolkata');

class PageBase 
{
	static $email_title = 'MyHest';
	static $reset_pass_url = 'http://wishywashy.siddhiglobal.net/horse_api/reset_password.php';
	static $baseurl = 'http://wishywashy.siddhiglobal.net/horse_api/';
	static $config = array(
	   'protocol'  => 'ServerProtocol.Imap4',
	   'smtp_host' => 'webmail.pandoraict.com',
	   'smtp_port' => 993,
	   'smtp_user' => 'jobs@pandoraict.com',
	   'smtp_pass' => 'job#@1234',
	   'mailtype'  => 'html',
	   'charset' => 'iso-8859-1',
	      'wordwrap' => TRUE
   	);

	public static function GetLocalDate()
	{
		return new DateTime("now", new DateTimeZone('asia/kolkata'));
	}
	
	public static function convertToDBString($input)
	{
		$input = trim($input);
		return ($input == '' || $input == 'undefined' || $input == 'null') ? NULL : $input;
	}	

	//generate serial number
	public static function generateSerialNo($oldno)
	{
		$oldno == 0 ? $oldno = 1000 : $oldno = (int)$oldno + 1 ;
		return $oldno;
	}

	//generate random number
	public static function generaterandomno($digits)
	{
		return rand(pow(10, $digits-1), pow(10, $digits)-1);
	}

	public function sendEmail($data)
	{
		$this->load->library('email');
		$this->email->initialize(PageBase::$config);
		// $this->email->set_mailtype("tls");
		$this->email->set_newline("\r\n");
		$this->email->to($data['to']);
		$this->email->from('jobs@pandoraict.com',PageBase::$email_title);
		$this->email->subject($data['subject']);
		$this->email->message($data['html']);
		//Send email
		$this->email->send();
		//echo  $this->email->print_debugger();	exit;	
	}
	
	public static function uploadImages($base64_string,$path)
	{
		$folderPath = FCPATH.$path;
		$img = str_replace('data:image/png;base64,', '', $base64_string);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$basename=uniqid();
		$file = $folderPath . $basename . '.png';
		file_put_contents($file, $data);
        return  $basename . '.png';
	}

// 	public static function uploadImages($imagearr,$index,$path)
// 	{
// 		$file = '';
// 		$uploadpath = FCPATH.$path;
// 		$fparray = explode(".",$imagearr["name"][$index]); 
// 		$fileName = uniqid().".".$fparray[1];
// 		$uploadfile = $uploadpath . $fileName;	
// 		if(move_uploaded_file($imagearr["tmp_name"][$index], $uploadfile) == true)
// 		{
// 			$file = $uploadfile;
// 		}
// 		return $path.$fileName;						
// 	}

}
?>