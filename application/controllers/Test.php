<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Test extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->model('app/other_model');
	    $this->load->model('common_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//add vendor
	public function addvendor_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['prn'] = trim($this->post('prn'));
		$data['date'] = trim($this->post('date'));
		$data['gender'] = trim($this->post('gender'));
		$data['role_id'] = trim($this->post('role_id'));
		$data['address'] = trim($this->post('address'));
		$data['mobile_no'] = trim($this->post('mobile_no'));
		$data['adhar_no'] = trim($this->post('adhar_no'));
		$data['email'] = trim($this->post('email'));
		$data['working_area'] = trim($this->post('working_area'));
		$data['type'] = 1;
		
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'user',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add subvendor
	public function addsubvendor_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['code'] = trim($this->post('code'));
		$data['date'] = trim($this->post('date'));
		$data['gender'] = trim($this->post('gender'));
		$data['role_id'] = trim($this->post('role_id'));
		$data['address'] = trim($this->post('address'));
		$data['mobile_no'] = trim($this->post('mobile_no'));
		$data['adhar_no'] = trim($this->post('adhar_no'));
		$data['email'] = trim($this->post('email'));
		$data['working_area'] = trim($this->post('working_area'));
		$data['type'] = 2;
		
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'user',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add newspaper
	public function addnewspaper_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['paper_name'] = trim($this->post('paper_name'));
		$data['district'] = trim($this->post('district'));
		$data['publication_house'] = trim($this->post('publication_house'));
		$data['dealer_name'] = trim($this->post('dealer_name'));
		$data['language'] = trim($this->post('language'));
		$data['paper_type'] = trim($this->post('paper_type'));
		
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'newspaper',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get newspaper by type
	public function getnewspaperbytype_post()
	{
		$authcode = $this->post('authcode');
		$data['user_id'] = $this->post('user_id');
		$data['type_id'] = trim($this->post('type_id'));		
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->sample_model->getNewspaperByType($data,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'success','record'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add pricemaster
	public function addnewspaperprice_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['newspaper_id'] = trim($this->post('newspaper_id'));
		$data['monday'] = trim($this->post('monday'));
		$data['tuesday'] = trim($this->post('tuesday'));
		$data['wednesday'] = trim($this->post('wednesday'));
		$data['thursday'] = trim($this->post('thursday'));
		$data['friday'] = trim($this->post('friday'));
		$data['saturday'] = trim($this->post('saturday'));
		$data['sunday'] = trim($this->post('sunday'));
		
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'newspaper_price',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add vendor stock
	public function addvendorstock_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['vendor_id'] = trim($this->post('vendor_id'));
		$data['newspaper_id'] = trim($this->post('newspaper_id'));
		$data['date'] = trim($this->post('date'));
		$data['monday'] = trim($this->post('monday'));
		$data['tuesday'] = trim($this->post('tuesday'));
		$data['wednesday'] = trim($this->post('wednesday'));
		$data['thursday'] = trim($this->post('thursday'));
		$data['friday'] = trim($this->post('friday'));
		$data['saturday'] = trim($this->post('saturday'));
		$data['sunday'] = trim($this->post('sunday'));
		$data['discount'] = trim($this->post('discount'));
		
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'vendor_stock',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add sub-vendor stock
	public function addsubvendorstock_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['subvendor_id'] = trim($this->post('subvendor_id'));
		$data['newspaper_id'] = trim($this->post('newspaper_id'));
		$data['date'] = trim($this->post('date'));
		$data['monday'] = trim($this->post('monday'));
		$data['tuesday'] = trim($this->post('tuesday'));
		$data['wednesday'] = trim($this->post('wednesday'));
		$data['thursday'] = trim($this->post('thursday'));
		$data['friday'] = trim($this->post('friday'));
		$data['saturday'] = trim($this->post('saturday'));
		$data['sunday'] = trim($this->post('sunday'));
		$data['discount'] = trim($this->post('discount'));
		
		$errormessage='';
		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'subvendor_stock',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get user information
	public function getuserinformation_post()
	{
		$authcode = $this->post('authcode');
		$data['user_id'] = $this->post('user_id');
		$data['code'] = $this->post('code');
		$data['username'] = $this->post('username');		
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$data['user_id'],$errormessage);
		if($auth == 1)
		{
			$result = $this->sample_model->getUserInformation($data,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'success','record'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get stock information from date
	public function getstockinfo_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$subvendor_id = $this->post('subvendor_id');
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->sample_model->getStockInformation($data,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'success','record'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get price information
	public function getpriceinformation_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');		
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->sample_model->getPriceInformation($user_id,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'success','record'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}


	//generate chalan
	public function generatechalan_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['subvendor_id'] = $this->post('subvendor_id');
		$data['total'] = $this->post('total');
		$data['date'] = $this->post('date');
		$data['day'] = $this->post('day');
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'chalan',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//add deposit amount
	public function adddepositamount_post()
	{
		$authcode = $this->post('authcode');
		$user_id = $this->post('user_id');
		$data['subvendor_id'] = $this->post('subvendor_id');
		$data['today_bill'] = $this->post('today_bill');
		$data['date'] = $this->post('date');
		$data['deposite_amount'] = $this->post('deposite_amount');
		$data['remaining_amount'] = $this->post('remaining_amount');
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$user_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->common_model->insertdata($data,'payment',$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'Record added succesfully.');
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get all newspaper
	public function getallnewspaper_post()
	{
		$authcode = $this->post('authcode');
		$vendor_id = $this->post('user_id');		
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$vendor_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->sample_model->getAllNewspaper($vendor_id,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'success','record'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get vendor stock
	public function getvendorstock_post()
	{
		$authcode = $this->post('authcode');
		$vendor_id = $this->post('user_id');		
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$vendor_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->sample_model->getAllVendorStock($vendor_id,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'success','record'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//get subvendor stock
	public function getsubvendorstock_post()
	{
		$authcode = $this->post('authcode');
		$vendor_id = $this->post('user_id');		
		$errormessage='';

		$auth = $this->common_model->checkLoginSession($authcode,$vendor_id,$errormessage);
		if($auth == 1)
		{
			$result = $this->sample_model->getAllSubVendorStock($vendor_id,$errormessage);
			if($result > 0)
			{
				$json = array("status"=>200,"message"=>'success','record'=>$result);
			}
			else
			{
				$json = array("status"=>200,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
	
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}



	// ********************************
	public function getNewspaperByType($data, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('newspaper');
		$this->db->where('active','1');
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('type_id',$data['type_id']);	
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getUserInformation($data, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('active','1');
		if($data['code'] !== '')
		{
			$this->db->where('code',$data['code']);
		}
		if($data['username'] !== '')
		{
			$this->db->where('name',$data['username']);
		}		
		
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getStockInformation($subvendor_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('subvendor_stock');
		$this->db->where('active','1');
		$this->db->where('subvendor_id',$subvendor_id);
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getPriceInformation($user_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('newspaper_price');
		$this->db->where('active','1');
		$this->db->where('user_id',$user_id);
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}


	public function getAllNewspaper($vendor_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('newspaper');
		$this->db->where('active','1');
		$this->db->where('vendor_id',$vendor_id);
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getAllVendorStock($vendor_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('vendor_stock');
		$this->db->where('active','1');
		$this->db->where('vendor_id',$vendor_id);
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getAllSubVendorStock($vendor_id, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('subvendor_stock');
		$this->db->where('active','1');
		$this->db->where('vendor_id',$vendor_id);
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

}