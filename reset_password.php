<?php
//database connection
$servername = 'localhost';
$username = 'siddh1lt_hrmuser';
$password = 'srZS1f25=P@&';
$dbname = 'siddh1lt_horse';
// $username = 'root';
// $password = '';
// $dbname = 'horse_db';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//token verification
$msg = '';
$token = $_GET['token'];
$sql = "SELECT id FROM user WHERE forget_pass_token='$token' "; 
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{ 
	$res_arr = $result->fetch_assoc();
	$id = $res_arr['id'];
}
else
{
	echo '<script>alert("Incorrect url");</script>';
	exit;
} 

?>
<!DOCTYPE html>
<html>
<head>
	<title>Reset Password</title>
	 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<style type="text/css">
		html,
		body{
		 height: 100%;
		}

		#cover {
		  background: #222 url('') center center no-repeat;
		  background-size: cover;
		  height: 100%;
		  display: flex;
		  align-items: center;
		}

		#cover-caption {
		  width: 100%;
		}
		.white{
			color: #fff;
		}
		.form-control{
			border-color: #E8BA43;
			background-color: #222;
			color: #fff;
		}

		.form-control:focus {
		    color: #fff;
		    background-color: #222;
		    border-color: #E8BA43;
		    outline: 0;
		    box-shadow: none;
		}

		.buttongradient{
        background: rgba(231,183,58,1);
        background: -moz-linear-gradient(left, rgba(231,183,58,1) 0%, rgba(244,202,113,1) 28%, rgba(254,217,154,1) 49%, rgba(246,206,122,1) 66%, rgba(236,191,81,1) 88%, rgba(231,183,58,1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(231,183,58,1)), color-stop(28%, rgba(244,202,113,1)), color-stop(49%, rgba(254,217,154,1)), color-stop(66%, rgba(246,206,122,1)), color-stop(88%, rgba(236,191,81,1)), color-stop(100%, rgba(231,183,58,1)));
        background: -webkit-linear-gradient(left, rgba(231,183,58,1) 0%, rgba(244,202,113,1) 28%, rgba(254,217,154,1) 49%, rgba(246,206,122,1) 66%, rgba(236,191,81,1) 88%, rgba(231,183,58,1) 100%);
        background: -o-linear-gradient(left, rgba(231,183,58,1) 0%, rgba(244,202,113,1) 28%, rgba(254,217,154,1) 49%, rgba(246,206,122,1) 66%, rgba(236,191,81,1) 88%, rgba(231,183,58,1) 100%);
        background: -ms-linear-gradient(left, rgba(231,183,58,1) 0%, rgba(244,202,113,1) 28%, rgba(254,217,154,1) 49%, rgba(246,206,122,1) 66%, rgba(236,191,81,1) 88%, rgba(231,183,58,1) 100%);
        background: linear-gradient(to right, rgba(231,183,58,1) 0%, rgba(244,202,113,1) 28%, rgba(254,217,154,1) 49%, rgba(246,206,122,1) 66%, rgba(236,191,81,1) 88%, rgba(231,183,58,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e7b73a', endColorstr='#e7b73a', GradientType=1 );
        
    }
	</style>
</head>
<body>
	<section id="cover">
	    <div id="cover-caption">
	        <div id="container" class="container">
	            <div class="row text-white">
	                <div class="col-12 col-lg-4 offset-lg-4">
	                    <div class="h3 text-center">Reset Password</div>
	                    <br>
	                    <div class="info-form">
	                        <form autocomplete="off">
	                        	<input type="hidden" name="record_id" id="record_id" value="<?php echo $id; ?>">
	                            <div class="form-group">
	                                <label class="white">New Password</label>
	                                <input type="password" class="form-control" name="new_pass" id="new_pass" maxlength="15" minlength="6" required>
	                            </div>
	                            <div class="form-group">
	                                <label class="white">Confirm Password</label>
	                                <input type="password" class="form-control" name="confirm_pass" id="confirm_pass" maxlength="15" minlength="6" required>
	                            </div>
	                            <div class="text-center">
	                            	<button type="button" id="submit" class="btn buttongradient">Save</button>
	                            </div>
	                            <?php
	                            echo $msg;
	                            ?>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>	
</body>
</html>

<script type="text/javascript">
	$('#submit').click(function() {
		var record_id = $('#record_id').val();
		var new_pass = $('#new_pass').val();
		var confirm_pass = $('#confirm_pass').val();
		if(new_pass == '' || new_pass == null)
		{
			alert('Password is required.');
			return false;
		}
		if(confirm_pass == '' || confirm_pass == null)
		{
			alert('Confirm password is required.');
			return false;
		}
		if(new_pass == confirm_pass)
		{
			$.ajax({
		        url: 'reset.php',
		        type: 'POST',
		        data: {
		        	record_id: record_id,
		            new_pass: new_pass,
		            confirm_pass: confirm_pass
		        },
		        success: function(res) {
		            if(res == 1)
		            {
		            	$('#record_id').val('');
		            	$('#new_pass').val('');
		            	$('#confirm_pass').val('');
		            	alert('Password reset successfully. Please Login to continue.');
		            }
		            else
		            {
		            	alert('Try again');
		            }

		        }               
		    });
		}
		else
		{
			alert('Password and confirm password not match');
		}
	});
</script>