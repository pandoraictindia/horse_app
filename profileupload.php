<?php
require 'db_connection.php';

$target_path = "images/profile_images/";
$image_name=uniqid().'_'.$_REQUEST['uid'].'_'.basename( $_FILES['profile_photo']['name']); 
$types = array('image/jpeg', 'image/jpg', 'image/png');

if (in_array($_FILES['profile_photo']['type'], $types)) 
{
	$user_id=$_REQUEST['uid'];
	$target_path = $target_path.$image_name;
	 
	if(move_uploaded_file($_FILES['profile_photo']['tmp_name'], $target_path)) 
	{
		$sql = "UPDATE user SET profile_photo = '$image_name' WHERE id='$user_id'";
		
		if ($conn->query($sql) === TRUE) 
		{
		    echo "New image uploaded successfully";
		} 
		else 
		{
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}
	} 
	else
	{	
	    echo "There was an error uploading the file, please try again!";
	}
}
else
{
	echo "Plese select only jpeg,jpg and png images.";
}
$conn->close();
?>